<!DOCTYPE html>
<html>
<head>
	<title>PHP Intro | Activity 1</title>
</head>
<body>
	<h1>Activity 1</h1>
	<?php
		function getFullAddress($country, $city, $province, $specificAddress){
			echo "You live in $country $city $province $specificAddress";
		}

		getFullAddress("Philippines", "Pasig", "Sta Lucia", "De Castro");
	?>
</body>
</html>